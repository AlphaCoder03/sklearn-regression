import operator
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.datasets import load_diabetes, load_boston, load_iris
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures


def diabetes():
    print('\nSelected dataset is \'Diabetes\'\n')
    dataset = load_diabetes()
    data_frame = pd.DataFrame(dataset.data, columns=dataset.feature_names)
    data_frame['QMODP'] = dataset.target
    return data_frame


def boston():
    print('\nSelected dataset is \'Boston\'\n')
    dataset = load_boston()
    data_frame = pd.DataFrame(dataset.data, columns=dataset.feature_names)
    data_frame['MEDV'] = dataset.target
    return data_frame


def iris():
    print('\nSelected dataset is \'Iris\'\n')
    dataset = load_iris()
    data_frame = pd.DataFrame(dataset.data, columns=dataset.feature_names)
    data_frame['QMODP'] = dataset.target
    return data_frame


def load_a_dataset(choice):
    data_sets = {1: diabetes, 2: boston, 3: iris}
    dataset = data_sets.get(choice)
    if dataset is None:
        raise Exception('Invalid Choice! Aborting Please try again...')
    else:
        return dataset()


def get_target_label(choice):
    targets = {1: 'QMODP', 2: 'MEDV', 3: 'QMODP'}
    target = targets.get(choice)
    if target is None:
        raise Exception('Invalid Choice! Aborting Please try again...')
    else:
        return target


def plot_and_compare_linear_vs_polynomial_models(data_frame, feature, target, degree):
    X = data_frame[[feature]]
    Y = data_frame[target]

    percent = int(input("\nEnter the percentage of records used for testing: "))
    if percent not in range(1, 100):
        raise Exception('Invalid Choice! Aborting Please try again...')

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=(percent / 100), random_state=5)

    poly_features = PolynomialFeatures(degree=degree)
    X_train_poly = poly_features.fit_transform(X_train)
    X_test_poly = poly_features.fit_transform(X_test)

    lin_model = LinearRegression()
    poly_model = LinearRegression()

    lin_model.fit(X_train, Y_train)
    poly_model.fit(X_train_poly, Y_train)

    Y_test_predict = lin_model.predict(X_test)
    Y_test_predict_poly = poly_model.predict(X_test_poly)

    lin_mse = mean_squared_error(Y_test, Y_test_predict)
    lin_r2 = r2_score(Y_test, Y_test_predict)

    poly_mse = mean_squared_error(Y_test, Y_test_predict_poly)
    poly_r2 = r2_score(Y_test, Y_test_predict_poly)

    print('\nLinear model performance for testing set')
    print("--------------------------------------")
    print('Coefficients : ', lin_model.coef_)
    print('Mean squared error : %.3f ' % lin_mse)
    print('Variance score : %.2f ' % lin_r2)
    print('--------------------------------------\n')

    plt.title('Linear Model')
    plt.xlabel(feature)
    plt.ylabel(target)
    plt.scatter(X_test[feature], Y_test, color='black', marker='^')
    sort_axis = operator.itemgetter(0)
    sorted_zip = sorted(zip(X_test[feature], Y_test_predict), key=sort_axis)
    x, y = zip(*sorted_zip)
    plt.plot(x, y, color='red', linewidth=3)
    plt.show()

    print("\nPolynomial model performance for testing set")
    print("--------------------------------------")
    print('Coefficients : ', poly_model.coef_)
    print('Mean squared error : %.3f ' % poly_mse)
    print('Variance score : %.2f ' % poly_r2)
    print('--------------------------------------\n')

    plt.title('Polynomial Model')
    plt.xlabel(feature)
    plt.ylabel(target)
    plt.scatter(X_test[feature], Y_test, color='black', marker='^')
    sort_axis = operator.itemgetter(0)
    sorted_zip = sorted(zip(X_test[feature], Y_test_predict_poly), key=sort_axis)
    x, y = zip(*sorted_zip)
    plt.plot(x, y, color='green', linewidth=3)
    plt.show()

    print('\n------------------* Results *--------------------\n')
    print('Comparison of Polynomial vs Linear:')
    print('** Mean squared error has {}, of that of linear'.format(increased_or_decreased(lin_mse, poly_mse)))
    print('** Variance score has {}, of that of linear'.format(increased_or_decreased(lin_r2, lin_r2)))
    print('\n------------------* END *--------------------\n')


def increased_or_decreased(x, y):
    if x == y:
        return 'NO CHANGE'
    elif y > x:
        return 'INCREASED by {}'.format(calculate_percentage(x, y))
    else:
        return 'DECREASED by {}'.format(calculate_percentage(x, y))


def calculate_percentage(x, y):
    percent = (abs(x - y) / x) * 100
    return '%.2f' % percent
