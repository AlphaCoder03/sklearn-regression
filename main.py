"""
04/12/2020
@author: Venugopal Reddy Alamuru
"""

import matplotlib.pyplot as plt
import seaborn as sns
import regression as model


print('\n*** DATASET LOADING STEP ***\n')
print('Hello User! Please choose a dataset to load:\n',
      '1) Diabetes',
      '2) Boston',
      '3) Iris')
choice = int(input('choose a dataset number: '))
data_frame = model.load_a_dataset(choice)
target = model.get_target_label(choice)
print('Displaying first 5 instance of the dataset:\n', data_frame.head(5))

print('\n*** DATA PROCESSING STEP ***\n')
print('Lets check if there any missing values in the dataset:\n')
print(data_frame.isnull().sum())
input("\nPress Enter to continue...\n")

print('\n** DATA ANALYSIS STEP ***\n')
print('Lets observe the distribution of the target variable \'{}\''.format(target))
sns.distplot(data_frame[target])
plt.title('Distribution of {}'.format(target))
plt.savefig('distribution_of_target_{}.png'.format(target))
plt.show()
input("\nPress Enter to continue...\n")

print('\nLets create a correlation matrix that measures the linear relationships between the variables.')
correlation_matrix = data_frame.corr().round(2)
sns.heatmap(data=correlation_matrix, annot=True)
plt.title('Co-relation heatmap of features & {}'.format(target))
plt.savefig('co-relation_heat_map_of_features_and_{}.png'.format(target))
plt.show()

print('\nAfter observing heat map, please enter the feature number '
      'which has high correlation wih our target: \'{}\'\n'.format(target))
print('Features:')
feature_list = list(data_frame.columns)[:-1]
for i in feature_list:
    print('{}) {}'.format(feature_list.index(i)+1, i))
fNum = int(input("\nEnter a feature number to continue...\n")) - 1
if fNum not in range(0, len(feature_list)):
    raise Exception('Invalid Choice! Aborting Please try again...')
feature = feature_list[fNum]

print('\nObserve the plotting between {} vs {}'.format(feature, target))
plt.scatter(data_frame[feature], data_frame[target], marker='x', color='red')
plt.xlabel(feature)
plt.ylabel(target)
plt.title('Relation between feature: {} & target: {}'.format(feature, target))
plt.savefig('relation_between_{}_and_{}.png'.format(feature, target))
plt.show()
input("\nPress Enter to continue...\n")

print('\n*** PREPARING DATA FOR TRAINING & TESTING THE MODEL ***\n')
degree = int(input('Enter the polynomial degree you want to you: '))
if degree < 2:
    raise Exception('Invalid Choice! Aborting Please try again...')

model.plot_and_compare_linear_vs_polynomial_models(data_frame, feature, target, degree)
